package com.dot.micro.shop.api;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.github.ggeorgovassilis.springjsonmapper.spring.SpringRestInvokerProxyFactoryBean;

@Configuration
public class ShopApiConfiguration {
	
	
	@Bean
	SpringRestInvokerProxyFactoryBean ShopService() {
		SpringRestInvokerProxyFactoryBean proxyFactory = new SpringRestInvokerProxyFactoryBean();
		proxyFactory.setBaseUrl("http://http://localhost:5555/shop/");
		proxyFactory.setRemoteServiceInterfaceClass(ShopResource.class);
		return proxyFactory;
	}

}
