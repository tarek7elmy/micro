package com.dot.micro.shop.svc.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dot.micro.inventory.api.Inventory;
import com.dot.micro.inventory.api.InventoryAPI;
import com.dot.micro.shop.api.ShopResource;


@RestController
@RequestMapping("/shop")
public class ShopController implements ShopResource{
	
	
	 @Autowired
	 InventoryAPI inventoryService;
	 

	@Override
	public String shopServiceGreating() {
		// TODO Auto-generated method stub
		
		return "greatings from Shop Service"+inventoryService.getAll();
	}

	

}
