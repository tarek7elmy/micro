package com.dot.micro.shop.svc;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.dot.micro.inventory.api.InventoryAPI;
import com.github.ggeorgovassilis.springjsonmapper.spring.SpringRestInvokerProxyFactoryBean;

@Configuration
public class InventoryApiConfiguration {
	
	
	@Bean
	SpringRestInvokerProxyFactoryBean InventoryService() {
		SpringRestInvokerProxyFactoryBean proxyFactory = new SpringRestInvokerProxyFactoryBean();
		proxyFactory.setBaseUrl("http://localhost:4747/");
		proxyFactory.setRemoteServiceInterfaceClass(InventoryAPI.class);
		return proxyFactory;
	}

}
