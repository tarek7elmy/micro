package com.dot.micro.service.customer.svc.rest;

import java.util.ArrayList;
import java.util.List;


import org.springframework.stereotype.Service;

import customer.service.rest.api.Customer;
import customer.service.rest.api.CustomerResource;
import customer.service.rest.api.NoCustomerFoundException;


@Service
public class CustomerImpl implements CustomerResource{

	@Override
	public List<Customer> getAllCustomers() {
		// TODO Auto-generated method stub
		
		List<Customer> result=new ArrayList<Customer>();
		
		
		result.add( new Customer("Customer1","Customer adress1"));
		result.add( new Customer("Customer2","Customer adress2"));
		result.add( new Customer("Customer3","Customer adress3"));
		result.add( new Customer("Customer4","Customer adress4"));
		
		return result;
	}

//	@Override
//	public Customer getCustomer(long id) throws NoCustomerFoundException {
//		// TODO Auto-generated method stub
//		return new Customer("Customer1","Customer adress1");
//	}

}
