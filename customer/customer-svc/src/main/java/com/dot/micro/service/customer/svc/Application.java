package com.dot.micro.service.customer.svc;

import javax.validation.constraints.Digits;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;

import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;


@SpringBootApplication(scanBasePackages = "com.dot.micro")
@ComponentScan({ "com.dot.micro"})

public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	@Autowired
	private DiscoveryClient discoveryClient;
	
	@Bean
	public DiscoveryClient getDiscoveryClient() {
		
		return discoveryClient;
	}
	
	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}
	
	/**
	    * Enables returning JSON from CXF REST services, in particular collections that
	    * are not wrapped in other containing objects.
	    * 
	    * @return an instance of the {@link JacksonJsonProvider}
	    */
	   @Bean
	   @ConditionalOnMissingBean
	   public JacksonJsonProvider jsonProvider() {
	      JacksonJaxbJsonProvider aProvider = new JacksonJaxbJsonProvider();
	      aProvider.setMapper(new ObjectMapper());
	      return aProvider;
	   }
	   
	   
	   
	   
}
