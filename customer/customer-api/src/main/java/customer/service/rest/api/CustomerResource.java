package customer.service.rest.api;

import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import java.util.List;


@Path("/")
@Produces(MediaType.APPLICATION_JSON)

public interface CustomerResource {	
	@GET
	@Path("/all")
	List<Customer> getAllCustomers();
	
//	@GET
//	@Path("{id}")
//	Customer getCustomer(@PathParam("id") long id) throws NoCustomerFoundException;
}
