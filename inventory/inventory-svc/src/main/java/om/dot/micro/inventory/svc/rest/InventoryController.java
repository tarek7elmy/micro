package om.dot.micro.inventory.svc.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dot.micro.inventory.api.Inventory;
import com.dot.micro.inventory.api.InventoryAPI;


@RestController
//@RequestMapping("/inventory")
public class InventoryController implements InventoryAPI{
	
	
	 @Autowired
	 InventoryAPI inventoryService;

	@Override
	public List<Inventory> getAll() {
		List<Inventory> result=new ArrayList<Inventory>();
		result.add(new Inventory("item 001",5));
		result.add(new Inventory("item 003",12));
		result.add(new Inventory("item 006",7));
		
		return result;
	}

	@Override
	public Optional<Inventory> getById(int id) {
		// TODO Auto-generated method stub
		
		
		return Optional.of(inventoryService.getAll().get(0));
	}

	@Override
	public void save(Inventory inventory, int id) {
		// TODO Auto-generated method stub
		
	}

}
