package com.dot.micro.inventory.api;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.github.ggeorgovassilis.springjsonmapper.spring.SpringRestInvokerProxyFactoryBean;

@Configuration
public class InventoryApiConfiguration {
	
	
	@Bean
	SpringRestInvokerProxyFactoryBean InventoryService() {
		SpringRestInvokerProxyFactoryBean proxyFactory = new SpringRestInvokerProxyFactoryBean();
		proxyFactory.setBaseUrl("http://http://localhost:4747/inventory/");
		proxyFactory.setRemoteServiceInterfaceClass(InventoryAPI.class);
		return proxyFactory;
	}

}
