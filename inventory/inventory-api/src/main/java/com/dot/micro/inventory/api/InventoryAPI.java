package com.dot.micro.inventory.api;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

//@RequestMapping("/api")
public interface InventoryAPI {
	
	
	@GetMapping("/")
    List<Inventory> getAll();
 
    @GetMapping("/{id}")
    Optional<Inventory> getById(@PathVariable int id);
 
    @PostMapping("/save/{id}")
    public void save(@RequestBody Inventory book, @PathVariable int id);
	
	
}
