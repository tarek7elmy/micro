package com.dot.micro.inventory.api;

public class Inventory {
	
	String itemName;
	int ItemQuntity ;
	public Inventory(String itemName, int itemQuntity) {
		super();
		this.itemName = itemName;
		ItemQuntity = itemQuntity;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public int getItemQuntity() {
		return ItemQuntity;
	}
	public void setItemQuntity(int itemQuntity) {
		ItemQuntity = itemQuntity;
	}
	
	

}
